Web en Flask
============
Gabriel Longás
--------------

### Instalación y configuración
* Instalamos virtualenv para poder crear entornos de desarrollo de Python (**$ sudo pip install virtualenv**)
* Creamos nuestro entorno (**$ virtualenv web-flask**) y lo inicializamos (**$ . web-flask/bin/activate**)
* Creamos una carpeta llamada app dentro de web-flask y metemos ahí los archivos
* Instalamos las depencias (**$ pip install -r requirements.txt**)
* Si da error al instalar MySQL-Python hacemos lo siguiente (**$ sudo apt-get install python-dev libmysqlclient-dev**) y volvemos a probar a instalar requirements.txt
* Para configurar MySQL instalamos (**$ sudo apt-get install mysql-server mysql-client**)
* Entramos en MySQL por terminal (**$ mysql -u username -p**) y creamos una bbdd llamada webflask (**CREATE DATABASE webflask;**)
* Entramos a la bbdd creada anteriormente y creamos las tablas users y contacts con el archivo create_tables.sql
* Abrimos el archivo **conf.py** y modificamos la variable **SQLALCHEMY_DATABASE_URI**
* Dentro de la carpeta recien creada ejecutamos la aplicación (**$ python app.py**)
* Entramos a nuestro navegador y nos metemos en **localhost:5000**