# -*- coding: utf-8 -*-

# Clave para formularios
SECRET_KEY = "clave secreta"

# Email
MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'tucorreo@gmail.com'
MAIL_PASSWORD = 'tu password'

# Conexión a la bbdd
SQLALCHEMY_DATABASE_URI = 'mysql://tuuser:tupass@localhost/webflask'

# Recaptcha
RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = '6Ld4ReoSAAAAAN1ajNQnVRXwizcc7ZCcLQK354Oe'
RECAPTCHA_PRIVATE_KEY = '6Ld4ReoSAAAAAKYR39iaLoMDGoScGOIgPizpfevs'
RECAPTCHA_OPTIONS = {'theme': 'white'}